package Devgym;


interface Bounceable{
	void bounce();
	void setBounceFactor(int bf);
	private class BusinessLogic{
		int varl;
		float var2;
		double result(int var1, float var2) 
		{
			return var1*var2;
		}
	}
}
class Test2 {
	public static void main(String[] args) 
	{
		System.out.println(new Bounceable.BusinessLogic().result(12,11.2));
	}

}
