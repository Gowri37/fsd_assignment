package Devgym;

public class TestQ1 {
	static void show()
	{
		System.out.println("super class show method");
	}
	
	static class StaticMethods
	
	{
	void show()
	{
		System.out.println("sub class show method");
	}
	}
	public static void main(String[] args)
	{
		TestQ1.show();
		new TestQ1.StaticMethods().show();
	}

}
