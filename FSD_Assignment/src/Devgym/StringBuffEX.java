package Devgym;

public class StringBuffEX {
	public static void main(String[] args)
	{
		StringBuilder sb= new StringBuilder("Hello ");
		
		System.out.println(sb.length());
		
		System.out.println(sb.capacity());
		sb.append("world ");
		System.out.println(sb);
		sb.append("2020");
		System.out.println(sb);
		sb.insert(6,"gowri ");
		System.out.println(sb);
		sb.reverse();
		System.out.println(sb);
		sb.delete(5, 10);
		System.out.println(sb);
		sb.replace(5, 11, "Triveni");
		System.out.println(sb);
		sb.ensureCapacity(60);
		System.out.println(sb.capacity());
	}

}
