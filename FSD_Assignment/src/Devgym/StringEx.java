package Devgym;

public class StringEx {
	public static void main(String[] args) {
		String s1 ="Split the string as directed";
		String[] a1 =s1.split(" ",2);
		String s2 ="Hello world, Hello Java, Hello JS";
		String s3 ="Hi";
		String s4 ="Hi";
		String s5= new String("Hi");
		String s6=s3.intern();
		System.out.println(s3==s6);
		for(String string:a1)
			System.out.println(string);
		System.out.println(s1.compareTo(s2));
		System.out.println(s2.contains("wo"));
		System.out.println(s2.replaceAll( "Hello", "Hi"));
		System.out.println(s2.replaceFirst( "Hello", "Hi"));
		System.out.println();
	}

}
