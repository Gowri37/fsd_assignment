package Devgym;
import java.util.Scanner;
public class ScannerExm {
public static void main(String[] args)
{
	Scanner s= new Scanner(System.in);
	System.out.println("What is the capital of the France?");
	s.next();
	System.out.println("What is 6 multiplied by 7>");
	s.nextInt();
	System.out.println("Enter a number between 0.0 and 1.0.");
	s.nextDouble();
	System.out.println("Is there anything else you would like to say?");
	s.next();
	System.out.println("Test the example multiple times");
	

}
	
	
}
