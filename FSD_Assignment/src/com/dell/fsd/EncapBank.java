package com.dell.fsd;

public class EncapBank {
	
	public String BankName;
	private int accountNo;
	private String accountName;
	private float accBal;
	private int phonenum;
	//getter and setter methods for AccountNo;
	public int getAccountNo()
	{
		return this.accountNo;
	}
	public void setAccountNo(int accountNo)
	{
		this.accountNo=accountNo;
			
	}
	//getter and setter methods for Accountname;
	public String getAccountName()
	{
		return this.accountName;
	}
	public void setAccountName(String name)
	{
		this.accountName=name;
	}
	//getter and setter methods for AccountBalance;
	public float getaccBal()
	{
		return this.accBal;
	}
	public void setaccBal(float bal)
	{
		this.accBal=bal;
	}
	//getter and setter methods for PhoneNumer;
	public int getPhone()
	{
		return this.phonenum;
	}
public void setPhone(int phone)
{
	this.phonenum=phone;
}
}
