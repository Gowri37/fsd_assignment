package com.dell.fsd;

public class DevGymJavaBasic {
	
	    public static void main(String[] args) {
	        double d = 1.0 / 0;
	        System.out.println(d);
	    }
	}



/*
Summary 


This question will give you deep understanding of using double/float values in java.






Background 


In the case of double/float division, the output is Infinity, since division by zero does not throw an ArithmeticException.

The reason for this is that double and float use the floating point arithmetic algorithm, which specifies a special values like �Not a number� or �infinity� while �divided by zero� as per IEEE 754 standards.*/
