package com.dell.fsd;

import java.util.Scanner;
public class SwitchStatement {

	
	public static void main(String[] args) {
		
		
		
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the choice as Admin / Vendor / user");
		String choice = s.nextLine();
		switch(choice)
		{
		case "admin" :
		case "Admin" :
		case "ADMIN" :
		case "1" :
		case "A" :
			System.out.println("Enter Admin name :");
			String aname = s.next();
			System.out.println("Enter Admin Id :");
			int aId= s.nextInt();
			System.out.println("Enter Admin Depart :");
			String aDept = s.next();
			System.out.println("Admin Data :"+"name :"+aname+"id :"+aId+"department :"+aDept);
			break;
		case "user" :
		case "User" :
		case "USER" :
		case "2" :
		case "B" :
			
			System.out.println("Enter User name :");
			String uname = s.next();
			System.out.println("Enter user Id :");
			int uId= s.nextInt();
			System.out.println("Enter User Depart :");
			String uDept = s.next();
			System.out.println("User Data :"+"name :"+uname+"id :"+uId+"department :"+uDept);
			break;
		case "Vendor" :
		case "VENDOR" :
		case "vendor" :
		case "3"	  :
			
			System.out.println("Enter vendor name :");
			String vname = s.next();
			System.out.println("Enter Vendor Id :");
			int vId= s.nextInt();
			System.out.println("Enter Vendor Business :");
			String vBs = s.next();
			System.out.println("Vendor Data :"+"name :"+vname+"id :"+vId+"Business :" +vBs+";");
			break;
		default :
			System.out.println("Invalid option");
			break;
			
			
		}
		s.close();
	}
	
}
