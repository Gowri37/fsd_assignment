package com.dell.fsd;

public class MethodCalling {
	//non static method need to create object
	void sampleCall()
	{
		//MyExample()  is default constructor/no
		MyExample me = new MyExample();
		me.myMethod();
		MyExample me2 = new MyExample(10,11);
		int z= me2.a;
		me2.showMethod();
	}

	public static void main(String[] args)
	
	{
				
		MyExample.myMethod();
		MyExample.yourMethod(12);
		MyExample.weMethod(12, 200f);
		MyExample.ourMethod();
	}
}
