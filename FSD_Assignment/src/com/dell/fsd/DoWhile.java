//pin example with do while- home work. --done.

package com.dell.fsd;
import java.util.Scanner;

public class DoWhile {
		
	
	public static void main(String[] args)
	{
		Scanner s = new Scanner(System.in);
		System.out.println("*************************************************************");
		System.out.println("Welcome to Bank of Dell, Please enter your PIN : ");
		int pin = s.nextInt();
		int  PIN= 1235;
		int tries = 0;		
		do 
		{
			System.out.println("Incorrect pin.Try carefully Again");
			System.out.println("Enter the PIN again");
			pin =s.nextInt();
			tries++;
		  }
		while(pin!=PIN && tries<3);
		
		if (pin==PIN)
			System.out.println("PIN Accepted,You can access your account");
			
		else
			
			System.out.println("No of attempts got over. Account locked");	
		s.close();
		
	}

}



