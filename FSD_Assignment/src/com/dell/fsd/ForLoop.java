//OTP example with for loop and PIN example with for loop.

package com.dell.fsd;

public class ForLoop {
public static void main(String[] args)
{
	//for(intialization;condition/expression;increement/decreement) {}
	for (int n =1; n<5;n++)
	{
		System.out.println("The number is "+n);
	}
}
}
