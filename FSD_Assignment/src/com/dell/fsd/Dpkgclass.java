package com.dell.fsd;
import com.dell.A.DataAccess;


public class Dpkgclass  
	{
		public static void main(String[] args) 
		{
			
			//d.prVar= 20;//can not be accessible as the access modifier is defined is private, which is accessible only within the class.
			//d.dVar=10;
			//d.proVar=19;
			//d.puVar=34;
			dbclass1.dbclassex();
			
		}
		public static class  dbclass1 extends DataAccess
		{
		
			static void dbclassex()
			{
			DataAccess d = new DataAccess();
			d.proVar=19;
			d.puVar=34;
			System.out.println(d.proVar);
			System.out.println(d.puVar);

			}
		}
		


}
