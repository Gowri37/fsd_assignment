package com.dell.fsd;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class GasonlineReceipt 
{
	
public static void main (String[] args)
{
	
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	Date date = new Date();
	Scanner s = new Scanner(System.in);
	System.out.println("Please select the shop name:");
	String SN = s.nextLine();
	System.out.println("No.of gallons required:");
	float NG = s.nextFloat();
	System.out.println("Price per gallon:");
	float price_gallon = s.nextFloat();
	float GT=NG*price_gallon;
	System.out.println("");
	System.out.println("+------------------------------+");
	System.out.println("|                              ");
	System.out.println("|     "+SN+"       	   ");
	System.out.println("|                              ");
	System.out.println("|  "+formatter.format(date)+"  ");
	System.out.println("|                              ");
	System.out.println("|Gallons:       "+NG   + "     ");
	System.out.println("|Price/Gallon: $"+price_gallon);
	System.out.println("|                              ");
	System.out.println("|Grand Total  : $"+GT   + "    ");
	System.out.println("|                              ");
	System.out.println("+------------------------------+");
	s.close();
}
}
