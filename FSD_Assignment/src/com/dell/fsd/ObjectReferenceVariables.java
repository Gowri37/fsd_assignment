package com.dell.fsd;

class Person{
	
		String name;
		int age;
		String address;
		
		void Display()
		{
			System.out.println("Person Information:"+name+"("+age +")"+"\n"+address);
		}
}

public class ObjectReferenceVariables {
	
	public static void main(String[] args)
	{
		Person p = new Person();
		Person q = new Person();
		p.name= "Nandu";
		p.age= 23;
		p.address="248,Sector 22,Noida";
		p.Display();
		q=p;
		q.name = "Mr.venkatesh";
		q.age=20;
		q.address="22, Mahanadi,IGNOU, MAIDAN Garhi";
		p.Display();
		q.Display();
		
	}
	
		
		
	}