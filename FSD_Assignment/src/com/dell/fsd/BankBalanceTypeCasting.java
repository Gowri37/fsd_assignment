package com.dell.fsd;

import java.util.Scanner;

public class BankBalanceTypeCasting {

	public static void main(String[] args)
	{
		/*Age --int
		Sal --float
		BB--doble
		AN--byte
		add  evertying -- int
		sub = higer -lower -->accbalance-accid -sal- age;-->byte
		div = an/bb- >byte
		multiply everything. --> int*/
		
		Scanner s = new Scanner(System.in);
		System.out.println("Age: ");
		int Age = s.nextInt();
		System.out.println("Salary: ");
		float Sal = s.nextFloat();
		System.out.println("Bank Balance: ");
		double BB =s.nextDouble();
		System.out.println("Account Id: ");
		byte AccId =s.nextByte();
		System.out.println("");
		/*int x =(int) (Age+Sal+BB+AccId);
		byte y=(byte)(AccId-Age);
		int z=(int)(Age*Sal*AccId*BB);
		byte p=(byte) (AccId/BB);*/
		//byte-->short-->int-->long-->float-->double(Widening) - Automatic 
		//double-->float-->long-->int-->short-->byte --(Narrowing)
		int x= Age+(int)Sal+(int)BB+AccId;
		//byte y=(byte)AccId-(byte)Age;
		byte y=(byte)(AccId-Age);
		int z=Age*(int)Sal*AccId*(int)BB;
		byte p= (byte)(AccId/BB);
		
		
		System.out.println("Addition -"+x);
		System.out.println("Substraction:"+y);
		System.out.println("Multiplication:"+z);
		System.out.println("Division:"+p);
		
		s.close();
	}
}
