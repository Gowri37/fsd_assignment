package com.dell.fsd;
import java.util.Scanner;

public class TernaryOp {
public static void main(String[] args) {
	System.out.println("Enter age of the user: ");
	Scanner s = new Scanner(System.in);
	int age =s.nextInt();
	int z;
	if (age>20) 
	{
	z=age;
	}else {
		z=0;
	}
//ternary operator
	z= age>20 ? age : 0;
	System.out.println(z);
	String loginMsg =(z!=0) ? "Login Success" : "Login Failed";
	System.out.println(loginMsg);
	s.close();
}
}


//Largest of 3 values using ternary.
//implement if-else using ternary.
