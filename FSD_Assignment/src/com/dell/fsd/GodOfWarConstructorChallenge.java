package com.dell.fsd;

import java.math.BigDecimal;

public class GodOfWarConstructorChallenge
	{
	    String name;

	    GodOfWarConstructorChallenge(String name)
	    {
	        this.name = name;
	    }

	    GodOfWarConstructorChallenge()
	    {
	        this(getName(1));
	    }

	    GodOfWarConstructorChallenge(int i) 
	    {
	        this(getName(2));
	    }

	    GodOfWarConstructorChallenge(Object i) 
	    {
	        this(getName((Integer) i));
	    }

	    static String getName(int index)
	    {
	        String name = new String[]{"Kratos", "Zeus","Poseidon", "Hades", "Athena"}[index];
	        return name;
	    }

	    public static void main(String... doYourBest) 
	    {
	        
	    	GodOfWarConstructorChallenge firstGod = new GodOfWarConstructorChallenge(Integer.valueOf(1));
	    	GodOfWarConstructorChallenge secondGod = new GodOfWarConstructorChallenge(Integer.valueOf(0));
	    	GodOfWarConstructorChallenge thirdGod = new GodOfWarConstructorChallenge(Integer.valueOf(4));
	        System.out.print(firstGod.name + " ");
	        System.out.print(secondGod.name + " ");
	        System.out.print(thirdGod.name);
	    }
	    
	}

	//Zeus Kratos Athena
