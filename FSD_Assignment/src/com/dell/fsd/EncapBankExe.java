package com.dell.fsd;

public class EncapBankExe {

	public static void main(String[] args) {
		EncapBank encap = new EncapBank();// step 1
		EncapBankOpr encapopr = new EncapBankOpr();
		//setting properties -- step 2
		encap.setAccountNo(1001);
		encap.setAccountName("Cany");
		encap.setaccBal(100000.01f);
		encap.setPhone(12345);
		encap.BankName="Meero Bank";
		//step 3 : gettting all the properties		
		System.out.println("Welcome to "+encap.BankName);
		System.out.println("AccountNumber is "+encap.getAccountNo());
		System.out.println("Account Name is"+encap.getAccountName());
		System.out.println("Account Phone number is"+encap.getPhone());
		System.out.println("Account Bal without Interest "+encap.getaccBal());
		//step 4: Bank Functionality --created another operation class and created an object for the it  by passing or taking user input
		EncapBank resp=encapopr.addInterest(encap);
		System.out.println("Account Bal with Interest :"+encap.getaccBal());
		System.out.println("-------------------------------------");
		

	}

}

//Bank A/c class-- bean class with setter and getter class, bank operation-show balance, deposit , add interest,withdraw;execution class- main class;

