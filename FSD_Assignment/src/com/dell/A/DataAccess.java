package com.dell.A;

public class DataAccess {
private static int prVar;//private variable - Accessible with in a class
static int dVar;//default variable -Accessible with in a package
protected static int proVar;//protected variable -Accessible with in a class,package,outstide the package with the help of inheritance.
public int puVar;//public variable - Accessible anywhere.
//inner class
private class PrivateClass{}
class Dclass{}
protected class proClass{}
public static class Pubclass{}
private void privateMethod() {};
static void defaultMethod() {};
protected  static int protectedMethod() {return 10;}
	public static void main(String[] args)
{
	System.out.println("data,"+prVar);
	System.out.println("data,"+dVar);
	 
 }
}
