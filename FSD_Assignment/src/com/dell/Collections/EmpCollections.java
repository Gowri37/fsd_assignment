package com.dell.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class EmpCollections {

	public static void main(String[] args) {
	Employee e = new Employee(101,"Cany",1200000.0f,"DEV");	
	Employee e1 = new Employee(101,"Rany",1200000.0f,"DEV");	
	Employee e2 = new Employee(101,"Rose",1200000.0f,"DEV");	
	Employee e3 = new Employee(101,"Nandu",1200000.0f,"DEV");	
	List<Employee> emplist= new ArrayList<Employee>();
	emplist.add(e);
	emplist.add(e1);
	emplist.add(e2);
	emplist.add(e3);
	emplist.add(1,e3);
	System.out.println(emplist.get(2).empName);
	Iterator<Employee> empIterator = emplist.iterator();
	//iterating a collection with iterator
	while(empIterator.hasNext())
	{
		System.out.println(empIterator.next().empName);
	}
	
	System.out.println("============================");
	//based on indexing- for with counter// 
	for(int i=0;i<emplist.size();i++)
	{
		System.out.println(emplist.get(i).empName);
	}
	
	System.out.println("============================");
	// Using for each
	for (Employee emp : emplist)
		{
			System.out.println(emp.empName + " "+emp.empDept );
		}
	System.out.println("============================");
	//Another type of for each called- Asynchronus for loop
	emplist.forEach((emp) ->{
		System.out.println(emp.empName + " "+emp.empNo );
	});
	}
}

/*Exercises:1) Create an arraylist of books collection;- name of the book, author, no.of pages, versions; using diversified for loops
2) create an arraylist of ticket collection;
3)*/
//user
//bankuser
//user collection
//bank user collection