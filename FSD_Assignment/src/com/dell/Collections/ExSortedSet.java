package com.dell.Collections;
import java.util.*;

public class ExSortedSet {
	
	public static void main(String[] args) {
		TreeSet<Integer> uD = new TreeSet<Integer>();
		uD.add(11);
		uD.add(110);
		uD.add(1110);
		uD.add(11);
		uD.add(30);
		System.out.println(uD);
		TreeSet<String> uS = new TreeSet<String>();
		uS.add("Ruth");
		uS.add("Esther");
		uS.add("Sarah");
		uS.add("Maria");
		System.out.println(uS.descendingSet());
		Iterator<Integer> iter= uD.iterator();
		while(iter.hasNext())
			System.out.println(iter.next());
		Iterator<String> siter=uS.descendingIterator();
		while(siter.hasNext())
			System.out.println(siter.next());
			
	}

}
