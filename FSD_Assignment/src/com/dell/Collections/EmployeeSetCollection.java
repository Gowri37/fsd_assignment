package com.dell.Collections;
import java.util.*;

public class EmployeeSetCollection {
	
	public static void main(String[] args) {
		
		Employee e = new Employee(101,"Cany",1200000.0f,"DEV");	
		Employee e1 = new Employee(101,"Rany",1200000.0f,"DEV");	
		Employee e2 = new Employee(101,"Rose",1200000.0f,"DEV");	
		Employee e3 = new Employee(101,"Nandu",1200000.0f,"DEV");	
		Set<Employee> empset =new HashSet<Employee>();
		empset.add(e1);
		empset.add(e2);
		empset.add(e3);
		empset.add(e);
		/*empset.forEach((emp) ->{
			System.out.println(emp.empName + " "+emp.empNo );
		});*/
		Iterator<Employee> itr = empset.iterator();
		while(itr.hasNext())
			System.out.println(itr.next());
		
	}

}


//create an hashset for unique email address,phone numbers and then combine both the sets into one.
//create a set collection  for bank user which holds Account number, bal, account type. 