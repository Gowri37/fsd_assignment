package com.dell.Collections;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;


public class DigitArrayList {
	public static void main(String[] args) {
		
		List <Integer> Num = new ArrayList<Integer>(Arrays.asList(101,102,103,104,105,106,107,108,109,110));
		List <Integer> Num2 = new ArrayList<Integer>(Arrays.asList(111,112,113,114,115,116,117,118,119,120));
		/*Num.add(101);
		Num.add(102);
		Num.add(103);
		Num.add(104);
		Num.add(105);
		Num.add(106);
		Num.add(107);
		Num.add(108);
		Num.add(109);
		Num.add(110);*/
		Num.addAll(Num2);
		System.out.println(Num);
		Num.set(5,112);
		System.out.println(Num);
		Num.add(5,200);
		System.out.println(Num);
		Num.remove(6);
		System.out.println(Num);
		Iterator <Integer> NumIterator=Num.iterator();
		while(NumIterator.hasNext())
		{
			System.out.println(NumIterator.next());
		}
		
		System.out.println(Num);
		
		
		
		
	}

}
/* Assignments: 
 * Digits Arraylist need to generate, digits as 101 to 110, replace element at 5th index
 * add an element at 5th index, remove element from 6th index,lastly iterate all digits.
 */