package com.dell.Collections;
import java.util.ArrayList;
import java.util.Iterator;
public class ListColln {

	public static void main(String[] args)
	{
		ArrayList <String> empNames=new ArrayList<String>();
		//adding elements
		empNames.add("Gowri");
		empNames.add("Triveni");
		empNames.add("Rany");
		empNames.add("Cany");
		System.out.println(empNames);
		//adding elements based on index note:remember shifting of 
		empNames.add(2,"Glory");
		System.out.println(empNames);
		//replace the elements
		empNames.set(2,"Shakeena");
		System.out.println(empNames);
		//gettting an element from the collection
		System.out.println(empNames.get(2));
		System.out.println(empNames.contains("raj"));
		//index of an element
		System.out.println(empNames.indexOf("Shakeena"));// have question
		//Iterate ArrayList
		Iterator<String> empIterator=empNames.iterator();
		while(empIterator.hasNext())
		{
			System.out.println(empIterator.next());
		}
		
		
		
		
	}

}

/* Assignments: 
 * Digits Arraylist need to generate, digits as 101 to 110, replace element at 5th index
 * add an element at 5th index, remove element from 6th index,lastly iter
 */
	
