package com.dell.Collections;

import java.util.*;

public class ListStack {
	
	public static void main(String[] args)
	{
		Stack<String> hero = new Stack<String>();
		hero.push("David");
		hero.push("Moses");
		hero.push("Eliah");
		hero.push("Paul");
		hero.push("Ishaiah");
		hero.addElement("jeremiah");
		hero.addElement("Israel");
		hero.pop();
		System.out.println(hero.search("Paul"));
		System.out.println(hero.peek());
		System.out.println(hero.capacity());
		System.out.println(hero);
		
}
}

//iterate it with the help of iterator and enumarator.