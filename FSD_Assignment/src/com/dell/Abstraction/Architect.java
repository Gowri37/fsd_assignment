package com.dell.Abstraction;
//Partial implementation - 
public abstract class Architect 
{
	//non- abstract method
	void design()
	{
		//implemented method
		
		System.out.println("Planning done by Architect");
	}
   //abstract or unimplemented method
	abstract void build();
	 
 
}
