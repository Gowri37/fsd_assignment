package com.dell.Abstraction;

public class ContractB extends Architect {
	void build()
	{
		System.out.println("House is build by Contract B");
	}

	void design()
	{
		System.out.println("House plan by an Architect");
	}
public static void main(String[] args)
{
	
	ContractB b = new ContractB();
	 b.build();
	 b.design();
}
}
