package com.dell.Abstraction;

public interface ArchitectInterF
{

	void design();
	void build();
	void plan();
	void interior();
	
}
