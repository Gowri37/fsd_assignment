package com.dell.Abstraction;

public class ContractC implements ArchitectInterF
{
	
	 public void build()
	{
		System.out.println("Implementing the build abstract method");
	}
	public void design()
	 {
		 System.out.println("Implementing the design abstract method form ArchitectInterF");
	 }
	 public void plan()
	 {
		 System.out.println("Implementing the plan abstract method from ArchitectInterF");
	 }
	public void interior()
	 {
		 System.out.println("Implementing the Interior abstract method from ArchitectInterF");
	 }
public static void main(String[] args)
{
	ArchitectInterF c = new ContractC();
	c.build();
	c.design();
	c.plan();
	c.interior();
}
}
