package com.dell.Inheritance;

public class InheritC extends InheritB {

	public void methodC()
	{
		System.out.println("Sub-Sub /child-child class method");
		
	}
	public static void main(String[] args)
	{
		InheritC c = new InheritC();
		c.methodA();//parent class method
		c.methodB();//parent class method
		c.methodC();//local class method.
	}
}


//generate 3 class
//Animal class
//intelligence and non-intelligence
//Animal
//women
