package com.dell.Inheritance;

public class InheritB extends Inherit_A {

	public void methodB()
	{
		System.out.println("Sub / Child class method");
	}
	public static void main(String[] args) {
		InheritB b = new InheritB();
		b.methodA();//Calling super or base or parent class method
		b.methodB();//Calling Local method.
	}
}
