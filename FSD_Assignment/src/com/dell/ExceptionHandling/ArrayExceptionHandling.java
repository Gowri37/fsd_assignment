package com.dell.ExceptionHandling;

public class ArrayExceptionHandling {

	public static void main(String[] args) 
	{
		try {
		int intArray[]= new int[] {} ;
		//String stringArray[] = new String[] {"one","two","three","four"};
		intArray[0]=100;
		intArray[1]=200;
		intArray[2] =300;
		intArray[3]=400;
		//stringArray[0]="101";
		for(int index =0;index<intArray.length;index++)
		{
		System.out.println(intArray[index]);	
		}
		/*for(int i =0;i<stringArray.length;i++)
		{
			System.out.println(stringArray[i]);
		}*/
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Handled ArrayIndexoutofBoundException");
			System.out.println(e);
		}
		
	}
}



//Array allows us store similar data in an order.
//Array Index starts from '0'
//Declartaion : 1) datatype arrayname[] new type[size]; or 2) datatype[] variablename;
//ex: int intArray[];or example:
//int[] intArray =new int[2];
/* byte byteArray[]
 * short shortArray[]
 * String Stringarray[]
 * employee empArray[]
 * object objectArray[]
 * */
