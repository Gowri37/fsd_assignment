package com.dell.ExceptionHandling;

import java.io.IOException;

//throws keyword is used to predict the exception or declaring an exception.
public class ThrowsKeyword {
static int bal =10000;
public static void deposit(int amount) throws IOException 
{
	if (amount<0)
	{
	
		throw new IOException("Invalid amount in input");
		
	}
	else {
		bal=bal+amount;
		System.out.println(bal);
	}
}

public static void depositcall(int amount) throws IOException
{
	deposit(amount);
}
public static void main(String[] args) throws IOException 
{
	//try {
		depositcall(10000);
	//}
	//catch(IOException e)
	//{
		//System.out.println("Handled Error");
		//e.printStackTrace();
	//}


}
}