package com.dell.ExceptionHandling;

public class FirstException {
	public static void main(String[] args)
	{
		try
		{
		int num1=100;
		int num2=0;
		System.out.println("first number:"+num1);
		int z = num1/num2;		
		System.out.println("second number:"+num2);
		System.out.println("Result:"+z);
		}
		catch(ArithmeticException e)// e is object of the class type ArithmaticException
		{
			System.out.println("Exception  Occured");
			e.printStackTrace();
		}
		System.out.println("Remaining statement");
		}

}


//"Exception" is the super class for all the exceptions classes
//if we already know the type of excepiton that we will get, we can directly use that class name for an instance : Arithmatic excepiton in this scenario
