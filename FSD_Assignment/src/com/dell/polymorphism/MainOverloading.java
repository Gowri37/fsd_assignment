package com.dell.polymorphism;

public class MainOverloading {
	//this metod is going to be called by JVM
public static void main(String[] args)
	{
	System.out.println("main(String[] args)");	
	main("Hi");
	}
public static void main(String args)
{
	System.out.println("main(String args)");
}
public static void main()
{
	System.out.println("main()");
}
public static void main(float args)
{
	System.out.println("main(float args)");
}
}

