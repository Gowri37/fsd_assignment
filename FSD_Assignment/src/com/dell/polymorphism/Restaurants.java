package com.dell.polymorphism;

public class Restaurants {
public static void takeORder()
{
	//logic
	System.out.println("Always Serve coffee");
}
private static int takeORder(int price)
{
	if (price>=20)
	{
	System.out.println("Serve Capuccino");
	return price;
	}
	else if(price>=50) 
	{
	System.out.println("Serve Bartista ");
	return price;
	}
	else if(price>=80) 
	{
	System.out.println("Serve Espresso ");
	return price;
	}
	return price;
}

public static float takeORder(float price, int time)
{
	if (price <=200 && time >20)
	{
	System.out.println("Serve Biryani");
		}
	else if( price >=200 && time>40)
	{
		System.out.println("Serve Mandi");
		
	}
	return price;
}
public static void main(String[] args)
{
	takeORder(200f,30);
}
}
