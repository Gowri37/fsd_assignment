package com.dell.DataStructures.Array;

public class ArrayDS {

	public static void main(String[] args) {
		
		//DS operations: Traversing, searching, sorting, merging, insertion/deletion. 
	
	int[] intArray = new int[7];
	intArray[0] =20;
	intArray[1] =35;
	intArray[2] =15;
	intArray[3] =2;
	intArray[4] =27;
	intArray[5] =-7;
	intArray[6] =-17;
	
	for(int i=0;i<intArray.length;i++)//forward Iteration
	{
		System.out.println("index :"+ i+"value:"+intArray[i]);
	}
	System.out.println("******************************************");
	for(int i=intArray.length-1;i>0;i--)//reverse iteration
	{
		System.out.println("index :"+ i+"value:"+intArray[i]);
	}
}
}
