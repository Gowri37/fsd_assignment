package com.dell.DataStructures.Array;

public class recursiveFactorial {
	
	public static void main(String[] args) {
		System.out.println(recursiveFact(7));
	}

	public static int recursiveFact(int num)
	{
		if(num ==0)
			return 1;
		return num*recursiveFact(num-1);
	}
}
