package com.dell.DataStructures.Array;

public class RecursionExample
{
	private static int count=0;
	public static void main(String[] args)
	{
		recursive();
	}
	public static void recursive()
	{
		if(count==5)
		{
			return;
		}
		System.out.println("I am recursive call");
		count++;
		recursive();
	}
}
