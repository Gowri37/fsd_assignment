package com.dell.DataStructures.Array;

public class BubbleSort 
{
	public static void main(String[] args)
{
	
	int[] intArray=  {20,35,-15,7,55,1,-22};
	
	for(int lastSortedIndex=intArray.length-1;lastSortedIndex>0;lastSortedIndex--)
	{
		for(int i=0;i<lastSortedIndex;i++)
		{
			if(intArray[i]>intArray[i+1])
			swap(intArray,i,i+1);
		}
	}
	for(int i=0;i<intArray.length;i++)//forward Iteration
	{
		System.out.println("index :"+ i+"value:"+intArray[i]);
	}
}

public static void swap(int[] array, int i, int j)
	{
		
		if(i==j)
			return ;
		int	temp= array[i];
		array[i]=array[j];
		array[j]=temp;
	}
}
