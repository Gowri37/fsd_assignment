package com.dell.DataStructures.Array;

public class Factorial {
	public static void main(String[] args) 
	{
	System.out.println(iterativeFactorial(7));	
	}

	public static long iterativeFactorial(int num)
	{
		if(num ==0)
		{
			return 1;
		}
	int fact =1;
	for(int i=1;i<=num;i++)
	{
		fact *=i;
	}
	return fact;
	}
}
