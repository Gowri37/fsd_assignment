package com.dell.DataStructures.Array;

import java.util.ArrayList;
import java.util.List;


public class EmployeeList {
	
	public static void main(String[] args)
	{
		Employee JohnSmith= new Employee("John","smith",1827);	
		Employee WillSmith =  new Employee("Will","smith",1837);
		Employee JohnMiller =  new Employee("John","Miller",1847);
		Employee BillyGraham =  new Employee("Billy","Graham",1857);
		Employee JohnGraham =  new Employee("John","Graham",1857);
		List<Employee> emplist= new ArrayList<Employee>();
		emplist.add(JohnSmith);
		emplist.add(WillSmith);
		emplist.add(JohnMiller);
		emplist.add(BillyGraham);
		emplist.forEach(elm->{System.out.println(elm);});
		System.out.println(emplist.size());
		emplist.add(2,JohnGraham);
		emplist.forEach(elm->{System.out.println(elm);});
		System.out.println(emplist.size());
		

}
}